﻿using DataRepoLib;
using PlayListLib;

namespace TestMySql;

public class Program
{

    static void Main(string[] args)
    {
        var artistRepo = new MySqlArtistRepo("192.168.2.73", "denis", "denis", "denis");
        artistRepo.Connect();
        artistRepo.UpdateNextId();
        
        artistRepo.Insert(new Artist("Patrice Michaud"));
        artistRepo.Insert(new Artist("Marie-Pier Arthur"));
        
        var artists = artistRepo.SelectAll();
        Console.WriteLine("Artistes :");
        Console.WriteLine(string.Join("\n", artists));

        var artist = artistRepo.Select(2);
        Console.WriteLine("Artiste 2 :");
        Console.WriteLine(artist);
        
        artistRepo.Close();
    }
}