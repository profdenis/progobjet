using MySql.Data.MySqlClient;

namespace DataRepoLib;

public abstract class MySqlDataRepo<TData, TKey> : IDataRepo<TData, TKey>
{
    public static string? GetStringOrNull(MySqlDataReader reader, string col)
    {
        return Convert.IsDBNull(reader[col]) ? null : reader.GetString(col);
    }

    public static int GetIntOrZero(MySqlDataReader reader, string col)
    {
        return Convert.IsDBNull(reader[col]) ? 0 : reader.GetInt32(col);
    }

    public string ConnectString { get; set; }
    protected MySqlConnection Conn;

    protected MySqlDataRepo(string connectString)
    {
        ConnectString = connectString;
    }

    protected MySqlDataRepo(string server, string db, string user, string password, int port = 3306)
        : this($"server={server};port={port};uid={user};pwd={password};database={db}")
    {
    }

    public bool Connect()
    {
        Conn = new MySqlConnection(ConnectString);
        try
        {
            Conn.Open();
            return true;
        }
        catch (MySqlException e)
        {
            return false;
        }
    }

    public void Close()
    {
        Conn.Close();
    }

    public abstract List<TData> SelectAll();

    public abstract TData? Select(TKey id);

    public abstract bool Insert(TData data);

    public abstract bool Update(TData data);

    public abstract bool Delete(TData data);

    public abstract bool Delete(TKey key);
}