using PlayListLib;

namespace DataRepoLib;

public class JsonArtistRepo : JsonDataRepo<Artist, int>
{
    public JsonArtistRepo(string fileName) : base(fileName)
    {
    }

    public override List<Artist> SelectAll()
    {
        var artists = base.SelectAll();
        if (artists.Count > 0)
        {
            var max = artists.Select(x => x.Id).Max();
            if (max >= Artist.NextId)
            {
                Artist.NextId = max + 1;
            }
        }

        return artists;
    }
    
    public override Artist? Select(int id)
    {
        List<Artist> artists = SelectAll();
        return artists.Find(x => x.Id == id);
    }

    public override bool Delete(int id)
    {
        Artist artist = new Artist
        {
            Id = id
        };
        return Delete(artist);
    }
    
}