using System.Data;
using MySql.Data.MySqlClient;
using PlayListLib;

namespace DataRepoLib;

public class MySqlArtistRepo : MySqlDataRepo<Artist, int>
{
    public static Artist CreateArtistFromReader(MySqlDataReader reader, string id = "id", string name = "name", string website = "website")
    {
        Artist artist = new Artist();
        artist.Id = reader.GetInt32(id);
        artist.Name = reader.GetString(name);
        artist.Website = GetStringOrNull(reader, website);
        return artist;
    }
    
    public MySqlArtistRepo(string connectString) : base(connectString)
    {
    }

    public MySqlArtistRepo(string server, string db, string user, string password, int port = 3306) 
        : base(server, db, user, password, port)
    {
    }

    public void UpdateNextId()
    {
        int nextId = 1;
        using MySqlCommand cmd = new MySqlCommand("SELECT max(id) AS nextId FROM artist", Conn);
        cmd.Prepare();

        using MySqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            nextId = GetIntOrZero(reader, "nextId");
            // Console.WriteLine(nextId);
        }

        Artist.NextId = nextId + 1;
    }


    public override List<Artist> SelectAll()
    {
        List<Artist> artists = new List<Artist>();
        using MySqlCommand cmd = new MySqlCommand("SELECT * FROM artist", Conn);
        cmd.Prepare();

        using MySqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            Artist artist = CreateArtistFromReader(reader);
            artists.Add(artist);
        }

        return artists;
    }

    public override Artist? Select(int id)
    {
        Artist artist = null;
        using MySqlCommand cmd = new MySqlCommand("SELECT * FROM artist WHERE id = @id", Conn);
        cmd.Parameters.AddWithValue("@id", id);
        cmd.Prepare();

        using MySqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            artist = CreateArtistFromReader(reader);
        }

        return artist;
    }

    public override bool Insert(Artist data)
    {
        using MySqlCommand cmd =
            new MySqlCommand("INSERT INTO artist(id, name, website) VALUES (@id, @name, @website)", Conn);
        cmd.Parameters.AddWithValue("@id", data.Id);
        cmd.Parameters.AddWithValue("@name", data.Name);
        cmd.Parameters.AddWithValue("@website", data.Website);
        cmd.Prepare();
        try
        {
            cmd.ExecuteNonQuery();
            return true;
        }
        catch (MySqlException e)
        {
            Console.WriteLine(e.Message);
            return false;
        }
    }

    public override bool Update(Artist data)
    {
        throw new NotImplementedException();
    }

    public override bool Delete(Artist data)
    {
        throw new NotImplementedException();
    }

    public override bool Delete(int key)
    {
        throw new NotImplementedException();
    }
}